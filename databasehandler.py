#! /usr/bin/env python3
#*.* coding:utf8 *.*

import queue
import logging
import threading
import time
import glob
import shutil
from datetime import datetime
from datetime import timedelta
now = datetime.now


#amount of credits granted by the add_credit badge
#this amout is obsolete, amout now has to be typed in each time
#add_credit_amount = 50
consume_credit_amount = -1


root = logging.getLogger()
logger = logging.getLogger(__name__)
logger.setLevel(root.level)

ch = logging.StreamHandler()
ch.setLevel(root.level)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

class DatabaseHandler(threading.Thread):
    """
    This is the thread that will handle the two databases
    The databases are "simple" pandas dataframes
        first database list the current status of the users
            each user entry has the following fields:
            "fullname","shortname","balance","main_badge","alt_badges",
            "account_creation_date","alt_badges_dates","last_update_date"

            "balance" is expressed in number of coffees that are already paid
            "main_badge" is the UID of the badge used for the creation of the 
                account
            "alt_badges" is a LIST of all the other badges registered to this
                account (before adding a new badge the system has to check if
                is it not already present in the database and it is probobly a
                good idea to check once in a while)

        second database lists the individual transactions
            each entry has the following fields:
            "fullname","badge","operation_type","amount","datetime",
            "previous_balance","resulting_balance",

            "operation_type" can be one of the following "coffee","payment",
            "account_creation"
    
    Note : "fullname" should be considered as the real account name, we should
    anticipate people with a given badge to be able to change accounts
    this ensures people can start in a grouped account and switch later on
    to a personal account whithout creating too many issues down the line

    timestamps are formated the following way yyyy-mm-ddThh-mm-ss 
    file are CSVs with "," separator and "#" will be used for comments
    and columns names will be added on the first line as a comment

    status files are written every day at the end of the day,
    timestamp of the status file corresponds to the time of the last status
    update 

    transaction files are written after each transaction with one file per day
    timestamp will correspond to the begining of the day
    """
    converters = {
                "fullname":str,
                "shortname":str,
                "balance":float,
                "main_badge":str,
                "alt_badges":eval, # for tuples
                "account_creation_date":str,
                "alt_badges_dates":eval,  # for tuples
                "last_update_date":str,
                "badge":str,
                "operation_type":str,
                "amount":float,
                "datetime":str,
                "previous_balance":float,
                "resulting_balance":float,
                }
    df_status_columns = [
                "fullname",
                "shortname",
                "balance",
                "main_badge",
                "alt_badges",
                "account_creation_date",
                "alt_badges_dates",
                "last_update_date",
    ]
    df_transactions_columns = [
               "fullname",
               "badge",
               "operation_type",
               "amount",
               "datetime",
               "previous_balance",
               "resulting_balance",
    ]

    def __init__(self,queue_state,queue_DB):
        super().__init__()

        #self.add_credit_amount = add_credit_amount
        self.consume_credit_amount = consume_credit_amount

        self.queue_state = queue_state
        self.queue_DB = queue_DB

        #we start by finding the lastest status file 
        #and all transaction files within the past month
        self.timestamp_format = '%Y-%m-%dT%H%M%S'

        #we load the latest status file if we can find it
        self.df_status = {}
        try:
            status_files = sorted(glob.glob("data/*_status.csv"))
            self.latest_status_file = status_files[-1]

            logger.warning(f"latest status file {self.latest_status_file}")
            # df status is now a dictionary of dictionaries
            # each entry is keyed by the fullname of the account
            #  and contains all the relevant values 
            self.load_status(self.latest_status_file)

            logger.debug(self.df_status)
            logger.warning(f"Status file successfully loaded")
        except (FileNotFoundError,IndexError,ValueError) as e:
            logger.warning(f"Status file not located, starting with a new one : {repr(e)}")

            entry = {
                "fullname" : "patient zero" ,
                "shortname" : "agent z",
                "balance" : 0,
                "main_badge" : "DEADBEEF",
                "alt_badges" : ("455524028460","000000001"),
                "account_creation_date" : str(now()),
                "alt_badges_dates" : (str(now()),str(now())),
                "last_update_date" : str(now()),
            }
            self.df_status[entry['fullname']] = entry
            entry = {
                "fullname" : "add_credit" ,
                "shortname" : "zombi",
                "balance" : 0,
                "main_badge" : "826167363963",
                "alt_badges" : (),
                "account_creation_date" : str(now()),
                "alt_badges_dates" : (),
                "last_update_date" : str(now()),
            }
            self.df_status[entry['fullname']] = entry

        self.df_transactions = []
        try:
            transaction_files = sorted(glob.glob("data/*_transactions.csv"))
            latest_transaction_files = transaction_files[-30:]

            self.load_transactions(latest_transaction_files)
            logger.debug(self.df_transactions)
            n = len(self.df_transactions)
            logger.warning(f"Transaction files successfully loaded, {n} transactions in the record")
        except (FileNotFoundError,IndexError,ValueError) as e:
            logger.warning(f"Transaction file not located, starting with a new one : {repr(e)}")
            #transaction database is a list of dict 
            #   "fullname",
            #   "badge",
            #   "operation_type",
            #   "amount",
            #   "datetime",
            #   "previous_balance",
            #   "resulting_balance",
            

        #lets update the status with the recent transactions
        for user_status in self.df_status.values():
            user_transactions = [t for t in self.df_transactions if t['fullname']==user_status["fullname"]]
            if len(user_transactions) == 0 :
                continue
            last_transaction = sorted(user_transactions,key=lambda x:x["datetime"])[-1]
            
            if last_transaction["datetime"] > user_status["last_update_date"]:
                user_status["balance"] = last_transaction["resulting_balance"]
                user_status["last_update_date"] = last_transaction["datetime"]

        #priming files
        self.status_logfile = open(
            f"data/{now().strftime(self.timestamp_format)}_status.csv","a")
        self.dump_status_to_file(self.df_status)

        self.transactions_logfile = open(
            f"data/{now().strftime(self.timestamp_format)}_transactions.csv","a")
        #we prime the transaction file with the header
        self.transactions_logfile.write(";".join(self.df_transactions_columns)+"\n")


        # fullname resolver is a dict that list all the UIDs and spits out the fullname
        # of the user, it serves as both a check if the badge is know and as a resovler
        self.fullname_resolver = {}
        for user_status in self.df_status.values():
            self.fullname_resolver.update({user_status["main_badge"]:user_status["fullname"]})
            if len(user_status["alt_badges"]) > 0:
                for UID in user_status["alt_badges"]:
                    self.fullname_resolver.update({UID:user_status["fullname"]})

        logger.debug(f"fullname_resolver : {self.fullname_resolver}")

    def load_status(self,filename):
        logger.debug(f"trying to load status file : {filename}")
        with open(filename) as f:
            header = f.readline().strip()
            try:
                assert header == ";".join(self.df_status_columns)
            except AssertionError as e: 
                logger.debug(f"hed : {header}")
                logger.debug(f"ref : {';'.join(self.df_status_columns)}")
                raise ValueError

            for line in f.readlines():
                if line[0] == "#":
                    continue
                elif len(line.strip()) == 0:
                    continue
                s = line.strip().split(';')
                try:
                    raw_entry ={
                    "fullname" : s[0],
                    "shortname": s[1],
                    "balance": s[2],
                    "main_badge": s[3],
                    "alt_badges": s[4],
                    "account_creation_date": s[5],
                    "alt_badges_dates": s[6],
                    "last_update_date": s[7],
                    }
                    entry = {k:self.converters[k](v) for k,v in raw_entry.items()}
                except Exception as e:
                    logger.debug(f"error  : {repr(e)}")
                    logger.debug(f'while parsing line : \n {line}')
                    raise ValueError

                self.df_status[entry["fullname"]] = entry

    def load_transactions(self,filenames):
        for filename in filenames:
            with open(filename) as f:
                logger.debug(f"loading transactions from : {filename}")
                header = f.readline().strip()
                try:
                    assert header == ";".join(self.df_transactions_columns)
                except AssertionError as e:
                    logger.debug(f"hed : {header}")
                    logger.debug(f"ref : {';'.join(self.df_transactions_columns)}")
                    raise ValueError

                for line in f.readlines():
                    if line[0] == "#":
                        continue
                    elif len(line.strip()) == 0:
                        continue
                    s = line.strip().split(';')
                    raw_entry ={
                    "fullname" : s[0],
                    "badge" : s[1],
                    "operation_type" : s[2],
                    "amount" : s[3],
                    "datetime" : s[4],
                    "previous_balance" : s[5],
                    "resulting_balance" : s[6],
                    }
                    try:
                        entry = {k:self.converters[k](v) for k,v in raw_entry.items()}
                    except Exception as e:
                        logger.debug(f"error  : {repr(e)}")
                        logger.debug(f'while parsing line : \n {line}')
                        raise ValueError

                    self.df_transactions.append(entry)

    def stop(self):
        """
        we've been asked to stop so we'll perform an emergency shutdown 
        """   
        self.status_logfile.flush()
        self.transactions_logfile.flush()
        self.transactions_logfile.close()
        self.status_logfile.close()

        #status file is just a normal status logfile
        self.status_logfile = open(
            f"data/{now().strftime(self.timestamp_format)}_status.csv","w")
        self.dump_status_to_file(self.df_status)
        self.status_logfile.close()

        #transaction file is called emergency because it duplicates all the
        # transactions that were kept in memory including very old ones
        with open(f"data/{now().strftime(self.timestamp_format)}_transactions_emergency.csv","w") as f:

            f.write(";".join(self.df_transactions_columns)+"\n")
            f.writelines(
                [";".join([str(_) for _ in entry.values()])+"\n" for entry in self.df_transactions])

        logger.info("Emergency stopping completed")

    def run(self):
        """
        running will consist of waiting for requests to appear in the queue
        and processing them
        and also there will be scheduled writing of the databases to files
        """
        while True:
            try:                    # see if something has been posted to Queue
                self.queue_dispatcher(self.queue_DB.get(timeout=1))
            except queue.Empty:
                #when nothing is going on we force the write of our data
                self.transactions_logfile.flush()

            #what follows is to cycle the transaction database files
            if now().hour < 1 : # check if we're past midnight
                previousdate = self.transactions_logfile.name.split('/')[-1].split("_")[0]
                previousdate = datetime.strptime(previousdate,self.timestamp_format) 

                #if now()-previousdate > timedelta(minutes=2): #for debug only, save the status every two minutes
                if now().date() != previousdate.date() : 
                    #check if previous file is from the same day
                    logger.debug(f"renewing  transactions_logfile : {self.transactions_logfile.name}")
                    self.transactions_logfile.flush()
                    self.transactions_logfile.close()
                    self.transactions_logfile = open(
                        f"data/{now().strftime(self.timestamp_format)}_transactions.csv",
                        "a")
                    #we prime the transacation file with the header
                    self.transactions_logfile.write(";".join(self.df_transactions_columns)+"\n")

            #what follows is the backup of the status database to files
            #if True : #for debug only, save the status every two minutes
            if now().minute < 1 : # check if we're at the begining of a new hour
                status_logfile_name =  self.status_logfile.name
                previousdate = self.status_logfile.name.split('/')[-1].split("_")[0]
                previousdate = datetime.strptime(previousdate,self.timestamp_format) 

                #if now()-previousdate > timedelta(minutes=2): #for debug only, save the status every two minutes
                if now()-previousdate > timedelta(minutes=30):
                    self.status_logfile.flush()
                    self.status_logfile.close()

                    if now().date() == previousdate.date() : 
                        #check if previous file is from the same day
                        shutil.move(status_logfile_name,"data/non_essential_logs/")

                    logger.debug(f"renewing status_logfile : {status_logfile_name}")
                    self.status_logfile = open(
                        f"data/{now().strftime(self.timestamp_format)}_status.csv","a")
                    self.dump_status_to_file(self.df_status)

    def dump_status_to_file(self,status):
        self.status_logfile.write(";".join(self.df_status_columns)+"\n")
        self.status_logfile.writelines(
            [";".join([str(_) for _ in entry.values()])+"\n" for entry in status.values()])
        self.status_logfile.flush()
        return

    def update_status(self,transaction_entry):
        user_entry = self.df_status[transaction_entry["fullname"]]
        logger.debug(f"user entry located")
        user_entry["balance"] += transaction_entry["amount"]
        logger.debug(f"balance updated")
        user_entry["last_update_date"] = transaction_entry["datetime"]
        logger.debug(f"date updated")
        logger.debug(f"updated status of {user_entry['fullname']}, new balance : {user_entry['balance']}")
    
    def write_transaction(self,entry):
        logger.debug(f"writing {entry}")
        try:
            logger.debug(f"appending")
            self.df_transactions.append(entry)
            logger.debug(f"appending done, writing")
            self.transactions_logfile.write(";".join([str(_) for _ in entry.values()])+"\n")
            logger.debug(f"writing done, updating status")
            self.update_status(entry)
            logger.debug(f"status updated")
        except (IOError,IndexError) as e:
            logger.warning(f"Error while trying to write a transaction : {e}")
            logger.debug(entry)
            raise IOError

    def query_transaction(self,badge_UID):
        """ 
        Function used when a transaction is initiated by the badge reader
        and needs to be checked. 
        if this is a special account ->  
        if the account is there a has balance -> 1
        if the account exists but doesn't have the proper balance -> 0
        if the account doesn't exist -> -1

        we return 
            transaction_result
        """

        try:
            userinfo = self.get_userinfo(badge_UID)
        except KeyError as e:
            logger.debug(f"{repr(e)}")
            logger.debug(f"transaction unknown UID : {badge_UID}")
            return "UID_unknown"

        logger.debug(f"got userinfo")

        try:
            if  userinfo["fullname"] == "add_credit":
                # we identified a special badge, we write a special transaction
                # and wait for the RFID handler to make the proper query next
                entry ={
                    "fullname" : userinfo["fullname"],
                    "badge" : badge_UID,
                    "operation_type" : "add_credit",
                    "amount" :  0,
                    "datetime" : str(now()),
                    "previous_balance" : userinfo['balance'],
                    "resulting_balance": userinfo['balance']-0,
                    # computing the balance is a bit useless but nvm
                }
                self.write_transaction(entry)
                return "transaction_add_credit_initiated"
            else:
                entry ={
                    "fullname" : userinfo["fullname"],
                    "badge" : badge_UID,
                    "operation_type" : "transaction",
                    "amount" :  self.consume_credit_amount,
                    "datetime" : str(now()),
                    "previous_balance" : userinfo['balance'],
                    "resulting_balance": userinfo['balance']+self.consume_credit_amount,
                }
                logger.debug(f"normal user, transaction entry prepared")
                self.write_transaction(entry)
                logger.debug(f"transaction entry written")
                if userinfo["balance"] >= 1:
                    return "transaction_OK"
                else :
                    return "transaction_OK_negative_balance"

        except IOError:
            logger.warning(f"Something went wrong trying to write the transaction")
            logger.warning(f"{userinfo}")
            return "transaction_NOK"

    def query_adduser(self,badge_UID,fullname,shortname):
        """
        Here we try to add a new user to the database
        we have to check first whether the user already exists or not
        """
        fullname_taken = (fullname in self.df_status.keys())
        UID_taken = (badge_UID in self.fullname_resolver.keys())

        if fullname_taken:
            return "name_taken"
        if UID_taken:
            return "UID_taken"
        else :  
            entry = {
                "fullname" : fullname ,
                "shortname" : shortname,
                "balance" : 0,
                "main_badge" : str(badge_UID),
                "alt_badges" : (),
                "account_creation_date" : str(now()),
                "alt_badges_dates" : (),
                "last_update_date" : str(now()),
            }
            self.df_status[fullname] = entry
            self.fullname_resolver[badge_UID]=fullname
            logger.debug(f"added user : {entry}")
            return "user_added"

    def query_add_UID_to_user(self,new_badge_UID,old_badge_UID=None,fullname=None):
        """
        Here we try to add a new UID to an existing user
        we have to check first whether the user already exists or not
        """
        logger.debug(f"trying to add UID {new_badge_UID} to {old_badge_UID}")
        UID_taken = (new_badge_UID in self.fullname_resolver.keys())
        if UID_taken:
            return "UID_taken"

        if fullname is not None:
            fullname_present = (fullname in self.df_status["fullname"])
            user_entry = self.df_status[fullname]

        elif old_badge_UID is not None:
            try :
                fullname = self.fullname_resolver[old_badge_UID]
            except KeyError:
                return "UID_invalid"
            user_entry = self.df_status[fullname]
        else : 
            logger.warning("Information missing when adding badge to existing user")
            logger.debug(f"badge UID {new_badge_UID}")
            return "missing_data"

        user_entry["alt_badges"] = user_entry["alt_badges"]+(new_badge_UID,)
        user_entry["alt_badges_dates"] = user_entry["alt_badges_dates"]+(str(now()),)
        user_entry["last_update_date"] = str(now())
        self.fullname_resolver[new_badge_UID]=user_entry["fullname"]
        logger.debug(f"updated user_entry : {user_entry} ")
        return "UID_added"

    def query_remove_UID_from_user(self,removed_badge_UID,other_badge_UID):
        """
        Here we try to remove a UID from a user 
        This should probably be an "admin only" function
        We'll take as a rule that a UID can only be removed if there are more
        than one registered for the user, if we're removing the main UID we
        then replace it with the oldest alt_UID
        """

        logger.debug(f"trying to remove UID {removed_badge_UID} from {other_badge_UID}")
        try :
            fullname = self.fullname_resolver[removed_badge_UID]
            fullname2 = self.fullname_resolver[other_badge_UID]
            if fullname != fullname2:
                return "UID_invalid"
        except KeyError:
            logger.warning("Trying to remove a UID that is not present")
            logger.debug("badge UID {removed_badge_UID}")
            return "UID_unknown"

        user_entry = self.df_status[fullname]
        if removed_badge_UID == user_entry["main_badge"]:
            if len(user_entry["alt_badges"]) == 0 :
                logger.warning("Trying to remove a UID without any alt_badges")
                logger.debug("badge UID {removed_badge_UID}")
                return "UID_nonremovable"

            user_entry["main_badge"] = user_entry["alt_badges"][0]
            user_entry["main_badge_date"] = user_entry["alt_badges_dates"][0]
            user_entry["alt_badges"] = user_entry["alt_badges"][1:]
            user_entry["alt_badges_dates"] = user_entry["alt_badges"][1:]
            user_entry["last_update_date"] = str(now())
            self.fullname_resolver.pop(removed_badge_UID)
            logger.debug(f"updated user_entry : {user_entry} ")
            return "UID_removed"

        else:
            i = user_entry["alt_badges"].index(removed_badge_UID)
            n = len(user_entry["alt_badges"])
            user_entry["alt_badges"] = (user_entry["alt_badges"][_] for _ in range(n) if _ != i)
            user_entry["alt_badges_dates"] = (user_entry["alt_badges_dates"][_] for _ in range(n) if _ != i)
            user_entry["last_update_date"] = str(now())
            self.fullname_resolver.pop(removed_badge_UID)
            logger.debug(f"updated user_entry : {user_entry} ")
            return "UID_removed"

    def query_addcredit(self,badge_UID,amount):
        try:
            userinfo =self.get_userinfo(badge_UID)
        except KeyError:
            logger.debug(f"query_addcredit unknown UID : {badge_UID}")
            userinfo = self.df_status["add_credit"]
            entry ={
                "fullname" : "add_credit",
                "badge" : badge_UID , #this is the wrong UID we received
                "operation_type" : "add_credit_canceled",
                "amount" :  0,
                "datetime" : str(now()),
                "previous_balance" : userinfo['balance'],
                "resulting_balance": userinfo['balance']+0,
            }
            self.write_transaction(entry)
            return "UID_unknown"

        if userinfo["fullname"] == "add_credit":
            entry ={
                "fullname" : "add_credit",
                "badge" : badge_UID,
                "operation_type" : "add_credit_canceled",
                "amount" :  0,
                "datetime" : str(now()),
                "previous_balance" : userinfo['balance'],
                "resulting_balance": userinfo['balance']+0,
            }
            self.write_transaction(entry)
            return "UID_invalid"

        try:
            entry ={
                "fullname" : userinfo["fullname"],
                "badge" : badge_UID,
                "operation_type" : "add_credit",
                "amount" :  amount,
                "datetime" : str(now()),
                "previous_balance" : userinfo['balance'],
                "resulting_balance": userinfo['balance']+amount,
            }
            self.write_transaction(entry)
            return "transaction_add_credit_done"

        except IOError:
            logger.warning(f"Something went wrong trying to write the transaction")
            logger.warning(f"{userinfo}")
            return "transaction_NOK"

    def get_userinfo(self,badge_UID):
        logger.info(f"getting userinfo of badge UID : {badge_UID}")

        fullname = self.fullname_resolver[badge_UID] 

        logger.info("UID found")
        try:
            user_entry = self.df_status[fullname] 
            logger.debug(f"-> name : {user_entry['fullname']}, balance : {user_entry['balance']}")
        except KeyError as e:
            logger.debug(f"error : {repr(e)}")
            raise Exception
        self.last_user_status = user_entry
        return user_entry

    def queue_dispatcher(self,queue_entry):    
        """
        without information we assume everything is a transaction
        query_transaction may proceed without issue or redirect to
        an other type of query, the RFID is in charge of making the
        proper query to continue
        
        in order to simplify the responses we send back to the RFIDhandler
        when the reply only entails to make a sound and go back to normal 
        operation, I indicated the sound to be made
        """
        logger.debug(f"reveived query {queue_entry}")
        if queue_entry["query_type"] == "transaction":
            res = self.query_transaction(queue_entry["badge_UID"])

            if res == "UID_unknown":
                self.queue_state.put({"query_type":"DB_answer",
                                      "message":"UID_unknown",
                                      "UID":queue_entry['badge_UID'],
                                     "sound":"boopboop",
                                    })
                logger.debug(f"unkown UID {queue_entry['badge_UID']}")

            if res == "transaction_add_credit_initiated":
                self.queue_state.put({"query_type":"DB_answer",
                                      "message":"transaction_add_credit_initiated",
                                     "sound":"bipbip"
                                    })

            if res == "transaction_OK":
                self.queue_state.put({"query_type":"DB_answer",
                                      "message":"transaction_OK",
                                      "balance":self.last_user_status["balance"],
                                      "fullname":self.last_user_status["fullname"],
                                     "sound":"beep",
                                    })

            if res == "transaction_OK_negative_balance":
                self.queue_state.put({"query_type":"DB_answer",
                                      "message":"transaction_OK_negative_balance",
                                      "balance":self.last_user_status["balance"],
                                      "fullname":self.last_user_status["fullname"],
                                      "sound":"beepboop",
                                    })

            if res == "transaction_NOK":
                self.queue_state.put({"query_type":"DB_answer",
                                      "message":"transaction_NOK",
                                      "sound":"boopboop",
                                    })

        elif queue_entry["query_type"] == "add_credit":
            res = self.query_addcredit(queue_entry["badge_UID"],queue_entry["amount"])
            if res == "transaction_add_credit_done":
                self.queue_state.put({"query_type":"DB_answer",
                                      "message":"transaction_add_credit_done",
                                      "added_credits":queue_entry["amount"],
                                      "balance":self.last_user_status["balance"],
                                      "fullname":self.last_user_status["fullname"],
                                      "sound":"beep",
                                    })
            if res == "UID_invalid":
                self.queue_state.put({"query_type":"DB_answer",
                                      "message":"UID_invalid",
                                      "sound":"boopboop",
                                    })
            if res == "UID_unknown":
                self.queue_state.put({"query_type":"DB_answer",
                                      "message":"UID_invalid",
                                      "sound":"boopboop",
                                    })

        elif queue_entry["query_type"] == "cancel_add_credit":
            self.query_addcredit(queue_entry["badge_UID"],0)

        elif queue_entry["query_type"] == "add_user":
            res = self.query_adduser(queue_entry["badge_UID"],
                                queue_entry['fullname'],
                                queue_entry['shortname'])

            if res == "name_taken":
                self.queue_state.put({"query_type":"DB_answer",
                                      "message":"name_taken",
                                      "sound":"boopboop",
                                    })
            if res == "UID_taken":
                self.queue_state.put({"query_type":"DB_answer",
                                      "message":"UID_taken",
                                      "sound":"boopboop",
                                    })
            if res == "user_added":
                self.queue_state.put({"query_type":"DB_answer",
                                      "message":"user_added",
                                      "UID":queue_entry["badge_UID"],
                                      "fullname":queue_entry['fullname'],
                                      "sound":"beepbeep",
                                    })

        elif queue_entry["query_type"] == "add_UID_to_user":
            res = self.query_add_UID_to_user(queue_entry['new_badge_UID'],
                                            queue_entry['old_badge_UID'])
            if res == "missing_data":
                self.queue_state.put({"query_type":"DB_answer",
                                      "message":"missing_data",
                                      "sound":"boopboop",
                                    })
            if res == "UID_taken":
                self.queue_state.put({"query_type":"DB_answer",
                                      "message":"UID_taken",
                                      "sound":"boopboop",
                                    })
            if res == "UID_added":
                self.queue_state.put({"query_type":"DB_answer",
                                      "UID":queue_entry['new_badge_UID'],
                                      "fullname":self.fullname_resolver[queue_entry['new_badge_UID']],
                                      "message":"UID_added",
                                      "sound":"beepbeep",
                                    })
            if res == "UID_invalid":
                self.queue_state.put({"query_type":"DB_answer",
                                      "message":"UID_invalid",
                                      "sound":"boopboop",
                                    })

        elif queue_entry["query_type"] == "remove_UID_from_user":
            res = self.query_remove_UID_from_user(queue_entry['removed_badge_UID'],
                                            queue_entry['old_badge_UID'])
            if res == "UID_unknown":
                # UID unknow message should be reserved to the case where we 
                # wnat the user to be able to create an account
                self.queue_state.put({"query_type":"DB_answer",
                                      "message":"UID_invalid",
                                      "sound":"boopboop",
                                    })
            if res == "UID_invalid":
                self.queue_state.put({"query_type":"DB_answer",
                                      "message":"UID_invalid",
                                      "sound":"boopboop",
                                    })
            if res == "UID_nonremovable":
                self.queue_state.put({"query_type":"DB_answer",
                                      "message":"UID_invalid",
                                      "sound":"boopboop",
                                    })
            if res == "UID_removed":
                self.queue_state.put({"query_type":"DB_answer",
                                      "UID":queue_entry['removed_badge_UID'],
                                      "fullname":self.fullname_resolver[queue_entry['old_badge_UID']],
                                      "message":"UID_removed",
                                      "sound":"beepbeep",
                                    })

        else:
            logger.error(f"Unprocessed query {queue_entry}")


def main():
    """
    This should be used to perform tests on the database handling
    I have never really used it yet
    """
    db = DatabaseHandler()

    db.df_status = read_csv("./test_status_db.csv")
    db.df_transactions = read_csv("./test_transactions_db.csv")

    #first I'll try to add a new transaction manually
    entry ={
    "fullname" : "test",
    "badge" : "241552152424441",
    "operation_type" : "transaction",
    "amount" :  -1,
    "datetime" : str(now()),
    "previous_balance" : 1,
    "resulting_balance" : 0,
    }
    db.write_transaction(entry)

    #we check that the transaction got added and that the status updated

def read_csv(*args):
    pass

if __name__ == '__main__':
    main()
