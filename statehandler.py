#!/usr/bin/env python3
#*.* encoding:utf8 *.*

import queue
import logging
import threading
import time
import queue
from datetime import timedelta
from datetime import datetime
now = datetime.now
import shutil

root = logging.getLogger()
logger = logging.getLogger(__name__)
logger.setLevel(root.level)

ch = logging.StreamHandler()
ch.setLevel(root.level)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

class StateHandler(threading.Thread):   
    def __init__(self,queue_state,queue_GUI,queue_DB,queue_beeper):
        super().__init__()
        
        #timeout values will be overwritten by main script
        self.state_timeout = 120 #sec
        self.add_credit_timeout = 60 # sec

        self.queue_GUI = queue_GUI
        self.queue_beeper = queue_beeper
        self.queue_DB = queue_DB
        self.queue_state = queue_state

        # states can be :
        # "transaction",
        # "add_user",
        # "add_credit",
        # "add_UID_to_user",
        # "remove_UID_from_user"
        self.state = "transaction"


        #the following are variables relevant only for some states
        self.last_badge_UID = None
        self.fullname = None
        self.shortname = None
        self.old_badge_UID = None
        self.addcredit_amount = None

    def update_state(self):
        """
        This function is not used anymore
        Everything is done in the run function
        """
        """
        This is where we handle the queue inputs and change the state 
        accordingly
        the last queue input always overwrite the current state
        """
        logger.error(f"using bad function : update_state")
        try: #blocking queue read for more reactivity
            queue_event = self.queue_state.get()
            logger.debug(f"received query: {queue_event}")
        except queue.Empty:     
            logger.debug(f"queue timeout")
            pass

        if "sound" in queue_event.keys():
            self.queue_beeper.put(queue_event)


        if queue_event["query_type"] == "DB_answer":
            expected_answers = {#ok
                "transaction_OK":"ok",
                "transaction_OK_negative_balance":"ok",
                "transaction_add_credit_done":"ok",
                "user_added":"ok",
                "UID_added":"ok",
                "UID_removed":"ok",
                #change state
                "transaction_add_credit_initiated":"change_state",
                #special error
                "UID_unknown":"error",
                #errors
                "transaction_NOK":"error",
                "UID_invalid":"error",
                "UID_taken":"error",
                "name_taken":"error",
                "missing_data":"error",
            }

            if queue_event["message"] == "transaction_add_credit_initiated":
                self.queue_GUI.put(queue_event)
                self.state = "add_credit"

            elif queue_event["message"] == "UID_unknown":
                # it's a special error that will be handled by the gui
                # We don't have anything special to do
                self.queue_GUI.put(queue_event)
                self.state = "transaction"

            elif queue_event["message"] in expected_answers.keys():
                #we just forward the queue event to the GUI
                self.queue_GUI.put(queue_event)
                self.state = "transaction"

        if queue_event["query_type"] == "GUI_query":
            if queue_event["message"] == "remove_UID_from_user":
                self.state = "remove_UID_from_user"

            elif queue_event["message"] == "add_UID_to_user":
                self.state = "add_UID_to_user"

            elif queue_event["message"] == "add_UID_to_user":
                self.state = "add_UID_to_user"

    def cancel_state(self,state):
        """
        Procedure to gracefully cancel whichever state we're in
        """

        logger.debug(f"canceling state : {state}")

        if self.state == "transaction":
            return

        elif self.state == "add_credit":
            self.queue_DB.put({
                "query_type":"cancel_add_credit",
                "badge_UID" : self.last_badge_UID,
            })
            self.queue_GUI.put({
                "query_type":"cancel",
            })
            self.queue_beeper.put({"query_type" : "sound",
                            "sound" : "boop"})

        elif self.state == "add_user":
            self.queue_GUI.put({
                "query_type":"cancel",
            })
            self.queue_beeper.put({"query_type" : "sound",
                            "sound" : "boop"})

        elif self.state == "removeUID_from_user":
            self.queue_GUI.put({
                "query_type":"cancel",
            })
            self.queue_beeper.put({"query_type" : "sound",
                            "sound" : "boop"})

    def run(self):
        #initialize timeout
        timeout = now()

        while True:
            try:
                queue_event = self.queue_state.get(timeout=0.2)
            except queue.Empty:
                #the following timeout is the state timeout
                if now() > timeout:
                    if self.state != 'transaction':
                        logger.debug(f"state timeout : {timeout}")
                        self.cancel_state(self.state)
                        self.state = "transaction"
                continue

            logger.debug(f"received query: {queue_event}")


            if "sound" in queue_event.keys():
                self.queue_beeper.put(queue_event)

            #it a two stage process, first we take the queue event and see if we should do
            # something based on our state 
            if self.state == "transaction" :
                if queue_event["query_type"] == "UID_read":
                    self.last_badge_UID = queue_event["UID"]
                    self.queue_DB.put({
                        "query_type" : "transaction",
                        "badge_UID" : queue_event["UID"],
                    })
                    continue

                elif queue_event["query_type"] == "change_state":
                    self.cancel(self.state)
                    self.state == queue_event["new_state"]
                    if self.state == "add_credit":
                        #add credit has a short timeout
                        timeout = now()+timedelta(seconds=self.add_credit_timeout)
                        #we make sure that the variables are initialized to the proper values
                        self.addcredit_amount = None
                        self.last_badge_UID = None
                    else:
                        # timeout for other states (typically adding a user) should
                        # be fairly long to allow the user to type their name
                        timeout = now()+timedelta(seconds=self.state_timeout)
                    continue

            elif self.state == "add_credit" :
                if queue_event["query_type"] == "UID_read" :
                    if self.addcredit_amount != None:
                        self.queue_DB.put({
                            "query_type":"add_credit",
                            "badge_UID" : queue_event["UID"],
                            "amount" : self.addcredit_amount,
                        })
                        self.state = "transaction"
                        self.addcredit_amount = None
                        self.last_badge_UID = None
                        continue
                    else:
                        self.last_badge_UID = queue_event["UID"]
                        

                if queue_event["query_type"] == "addcredit_amount": 
                    if self.last_badge_UID != None:
                        self.queue_DB.put({
                            "query_type":"add_credit",
                            "badge_UID" : self.last_badge_UID,
                            "amount" : queue_event["amount"],
                        })
                        self.state = "transaction"
                        self.addcredit_amount = None
                        self.last_badge_UID = None
                        continue
                    else:
                        self.addcredit_amount = queue_event["amount"]
                        self.queue_GUI.put({"query_type" : "alert",
                                    "message" : f"Present badge for the account to be credited ({queue_event['amount']} credits)",
                                    "timeout":self.add_credit_timeout } ) 
                        

            elif self.state == "add_UID_to_user" :
                if (self.last_badge_UID == None) and queue_event["query_type"] == "UID_read":
                    self.last_badge_UID = queue_event["UID"]
                    self.queue_GUI.put({"query_type" : "alert",
                                        "message" : "present badge of the account you wish to be attached to","timeout":self.add_credit_timeout})
                    timeout = now()+timedelta(seconds=self.add_credit_timeout)
                    continue

                elif (self.last_badge_UID != None) and queue_event["query_type"] == "UID_read":
                    self.queue_GUI.put({"query_type" : "alert",
                                    "message" : "Add request sent",
                                    "timeout":2 } ) 
                    self.queue_DB.put({
                        "query_type":"add_UID_to_user",
                        "old_badge_UID" : queue_event["UID"],
                        "new_badge_UID": self.last_badge_UID,
                    })
                    self.last_badge_UID = None
                    self.state = "transaction"
                    continue

            elif self.state == "remove_UID_from_user" :
                if (self.last_badge_UID == None) and queue_event["query_type"] == "UID_read":
                    self.last_badge_UID = queue_event["UID"]
                    self.queue_GUI.put({"query_type" : "alert",
                                    "message" : "present other badge of the account from which you'll be removed","timeout":self.add_credit_timeout})
                    continue

                elif (self.last_badge_UID != None) and queue_event["query_type"] == "UID_read":
                    self.queue_GUI.put({"query_type" : "alert",
                                    "message" : "Remove request sent",
                                    "timeout":2 } ) 
                    self.queue_DB.put({
                        "query_type":"remove_UID_from_user",
                        "removed_badge_UID": self.last_badge_UID,
                        "old_badge_UID":  queue_event["UID"],
                        "fullname": None,
                    })
                    self.last_badge_UID = None
                    self.state = "transaction"
                    continue

            if queue_event["query_type"] == "cancel":
                self.cancel_state(self.state)
                self.state = "transaction"

            elif queue_event["query_type"] == "DB_answer":
                expected_answers = {#ok
                    "transaction_OK":"ok",
                    "transaction_OK_negative_balance":"ok",
                    "transaction_add_credit_done":"ok",
                    "user_added":"ok",
                    "UID_added":"ok",
                    "UID_removed":"ok",
                    #change state
                    "transaction_add_credit_initiated":"change_state",
                    #special error
                    "UID_unknown":"error",
                    #errors
                    "transaction_NOK":"error",
                    "UID_invalid":"error",
                    "UID_taken":"error",
                    "name_taken":"error",
                    "missing_data":"error",
                }

                if queue_event["message"] == "transaction_add_credit_initiated":
                    self.queue_GUI.put(queue_event)
                    self.state = "add_credit"
                    timeout = now()+timedelta(seconds=self.add_credit_timeout)
                    self.addcredit_amount = None
                    self.last_badge_UID = None

                elif queue_event["message"] == "UID_unknown":
                    # it's a special errot that will handled by the gui
                    # We don't have anything special to do
                    self.queue_GUI.put(queue_event)
                    self.state = "transaction"

                elif queue_event["message"] in expected_answers.keys():
                    #we just forward the queue event to the GUI
                    self.queue_GUI.put(queue_event)
                    self.state = "transaction"

            elif queue_event["query_type"] == "GUI_query":
                if queue_event["message"] == "remove_UID_from_user":
                    self.state = "remove_UID_from_user"
                    timeout = now()+timedelta(seconds=self.add_credit_timeout)
                    self.last_badge_UID = None
                    self.queue_GUI.put({"query_type" : "alert",
                                    "message" : "present badge you want to remove",
                                    "timeout":self.add_credit_timeout } ) 

                elif queue_event["message"] == "add_UID_to_user":
                    self.state = "add_UID_to_user"
                    timeout = now()+timedelta(seconds=self.add_credit_timeout)
                    self.last_badge_UID = None
                    self.queue_GUI.put({"query_type" : "alert",
                                    "message" : "present badge you want to add",
                                    "timeout":self.add_credit_timeout } ) 

    def stop(self):
        return
