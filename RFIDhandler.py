#!/usr/bin/env python3
#*.* encoding:utf8 *.*

#sudo pip3 install mfrc522
#sudo pip3 install spidev

import queue
import logging
import threading
import time
from datetime import timedelta
from datetime import datetime
now = datetime.now


#minimun amout of time between two reads of the same badge for the read to be 
# counted, this is to avoid fast consecutive reads 
# this means badge needs to be completely off the reader for at least 2 seconds
# in order to be read again
same_read_cooldown = 2

#read cooldown, minimum amount of time between reads of two different badges 
# this is to avoid fast reads of two badges different badges  
read_cooldown = 1



try:
    import RPi.GPIO as GPIO
    from mfrc522 import SimpleMFRC522
    simulation_mode = False
except (RuntimeError,ImportError):
    simulation_mode = True

root = logging.getLogger()
logger = logging.getLogger(__name__)
logger.setLevel(root.level)

ch = logging.StreamHandler()
ch.setLevel(root.level)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


class RFIDHandler(threading.Thread):   
    """
    This module is a dumb module, it reads cards and send their GUID to the brain
    The only thing the module handles is to make sure that cards aren't read
    twice in a row
    """
    def __init__(self,queue_state):
        super().__init__()
        self.reader = SimpleMFRC522(bus=1,device=0,debugLevel="CRITICAL")
        self.queue_state = queue_state

    def run(self):
        last_UID = None
        last_readtime = now()
        while True:
            try:
                # the read function is blocking which is perfect for us 
                UID, text = self.reader.read()
                UID = str(UID)
            except IOError as e:
                logger.error(f"RFID read error : {repr(e)}")
                time.sleep(0.1)
                continue

            readtime = now()
            # here we try to debounce the RFID reading
            # a card should only be read once (in order to only produce one transaction)
            # and not continuously every 100ms
            # In order to ensure this we remember the last card we see and when
            # we saw it if the same card keeps being presented 
            # (eg : card is left on the reader)
            # then we ignore the reads, to be read again, the card has to be 
            # lifted for at least 2 seconds (the 2 seconds are counted from the
            # most recent time the card was seen)
            #
            # if the card is removed and an other one is introduced then there 
            # is a simple cool down of 1 second
            if (UID == last_UID) and (readtime-last_readtime < timedelta(seconds=same_read_cooldown)):
                logger.debug(f"same badge seen {last_readtime}")
                last_readtime = readtime
                time.sleep(0.1)
                continue

            if (UID != last_UID) and (readtime-last_readtime < timedelta(seconds=read_cooldown)):
                logger.debug(f"different badge but timeout not over")
                time.sleep(0.1)
                continue

            #if we passed the previous stuff, it means we have a valid UID read
            #we can thus forward the number to the brain
            logger.debug(f"last_UID : {last_UID}")
            logger.info(f"RFID read : {UID}")
            self.queue_state.put({
                "query_type":"UID_read",
                "UID": UID,
            })

            last_UID = UID
            last_readtime = readtime

            time.sleep(0.1)

    def stop(self):
        #GPIO.cleanup()
        pass


# if we're in simulation mode we overwrite the class with a dummy one that will send
# RFID reads at random interval (between 7 and 30 secs)
if simulation_mode == True:
    import random
    UIDs = {"826167363963":"add_credit",
            "455524028460":"patient zero",
            "000000001":"patient zero",
            "000000002":"unknown",
            
            }
    class RFIDHandler(threading.Thread):   
        def __init__(self,queue_state):
            super().__init__()
            self.queue_state = queue_state
            logger.critical(f"RFID starting in simulation mode")

        def run(self):
            timeout = now()+timedelta(seconds=5)
            while True:
                if now() > timeout:
                    n = random.sample([3,5,10],1)[0]
                    timeout = now()+timedelta(seconds=n)
                    UID = random.sample(list(UIDs),1)[0]
                    user = UIDs[UID]
                    logger.info(f"Fake RFID read : {UID} (user {user})")
                    logger.info(f"RFID read : {UID}")
                    self.queue_state.put({
                        "query_type":"UID_read",
                        "UID": UID,
                    })
                time.sleep(0.2)

        def run(self): #testing addcredit
            timeout = now()+timedelta(seconds=5)
            while True:
                UID = "826167363963"
                user = "add_credit"
                logger.info(f"Fake RFID read : {UID} (user {user})")
                logger.info(f"RFID read : {UID}")
                self.queue_state.put({
                    "query_type":"UID_read",
                    "UID": UID,
                })

                time.sleep(20)


                UID = "455524028460"
                user = "patient zero"
                logger.info(f"Fake RFID read : {UID} (user {user})")
                logger.info(f"RFID read : {UID}")
                self.queue_state.put({
                    "query_type":"UID_read",
                    "UID": UID,
                })

                time.sleep(5)

                UID = "455524028460"
                user = "patient zero"
                logger.info(f"Fake RFID read : {UID} (user {user})")
                logger.info(f"RFID read : {UID}")
                self.queue_state.put({
                    "query_type":"UID_read",
                    "UID": UID,
                })

                time.sleep(2)

                UID = "826167363963"
                user = "add_credit"
                logger.info(f"Fake RFID read : {UID} (user {user})")
                logger.info(f"RFID read : {UID}")
                self.queue_state.put({
                    "query_type":"UID_read",
                    "UID": UID,
                })

                time.sleep(20)

                UID = "00034039083094"
                user = "unknown"
                logger.info(f"Fake RFID read : {UID} (user {user})")
                logger.info(f"RFID read : {UID}")
                self.queue_state.put({
                    "query_type":"UID_read",
                    "UID": UID,
                })
                time.sleep(0.2)
        def stop(self):
            pass

