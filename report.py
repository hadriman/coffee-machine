#!/usr/bin/env python3
#*.* encoding:utf8 *.*

import smtplib
import logging
import threading
import time
from datetime import timedelta
from datetime import datetime
now = datetime.now

try:
    import RPi.GPIO as GPIO
    from mfrc522 import SimpleMFRC522
    simulation_mode = False
except (RuntimeError,ImportError):
    simulation_mode = True

# The first step is always the same: import all necessary components:
import smtplib
from socket import gaierror

# Specify the sender’s and receiver’s email addresses:
sender = "from@example.com"
receiver = "mailtrap@example.com"

# Type your message: use two newlines (\n) to separate the subject from the message body, and use 'f' to  automatically insert variables in the text
message = f"""\
Subject: Hi Mailtrap
To: {receiver}
From: {sender}
This is my first message with Python."""

try:
  # Send your message with credentials specified above
  with smtplib.SMTP(smtp_server, port) as server:
    server.login(login, password)
    server.sendmail(sender, receiver, message)
except (gaierror, ConnectionRefusedError):
  # tell the script to report if your message was sent or which errors need to be fixed
  print('Failed to connect to the server. Bad connection settings?')
except smtplib.SMTPServerDisconnected:
  print('Failed to connect to the server. Wrong user/password?')
except smtplib.SMTPException as e:
  print('SMTP error occurred: ' + str(e))
else:
  print('Sent')






root = logging.getLogger()
logger = logging.getLogger(__name__)
logger.setLevel(root.level)

ch = logging.StreamHandler()
ch.setLevel(root.level)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


class Report(threading.Thread):   
    """
    This module's job is to send a zip file with the status and the transactions
    files of the week
    """
    def __init__(self,queue_state):
        super().__init__()

    def run(self):
        last_UID = None
        last_readtime = now()
        while True:
            try:
                # the read function is blocking which is perfect for us 
                UID, text = self.reader.read()
                UID = str(UID)
            except IOError as e:
                logger.error(f"RFID read error : {repr(e)}")
                time.sleep(0.1)
                continue

            readtime = now()
            # here we try to debounce the RFID reading
            # a card should only be read once (in order to only produce one transaction)
            # and not continuously every 100ms
            # In order to ensure this we remember the last card we see and when
            # we saw it if the same card keeps being presented 
            # (eg : card is left on the reader)
            # then we ignore the reads, to be read again, the card has to be 
            # lifted for at least 2 seconds (the 2 seconds are counted from the
            # most recent time the card was seen)
            #
            # if the card is removed and an other one is introduced then there 
            # is a simple cool down of 1 second
            if (UID == last_UID) and (readtime-last_readtime < timedelta(seconds=same_read_cooldown)):
                logger.debug(f"same badge seen {last_readtime}")
                last_readtime = readtime
                time.sleep(0.1)
                continue

            if (UID != last_UID) and (readtime-last_readtime < timedelta(seconds=read_cooldown)):
                logger.debug(f"different badge but timeout not over")
                time.sleep(0.1)
                continue

            #if we passed the previous stuff, it means we have a valid UID read
            #we can thus forward the number to the brain
            logger.debug(f"last_UID : {last_UID}")
            logger.info(f"RFID read : {UID}")
            self.queue_state.put({
                "query_type":"UID_read",
                "UID": UID,
            })

            last_UID = UID
            last_readtime = readtime

            time.sleep(0.1)

    def stop(self):
        pass

