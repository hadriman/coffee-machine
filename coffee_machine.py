#! /usr/bin/env python3
#*.* coding:utf8 *.*

import PySimpleGUI as sg
import queue
import logging
import threading
import time
from textwrap import fill # fill breaks long strings with line breaks
from datetime import timedelta
from datetime import datetime
now = datetime.now
import string
import random

# logging
if True:
    #logging_level = logging.CRITICAL
    logging_level = logging.DEBUG

    logging.basicConfig(filename='logfile_coffee_machine.log', level=logging_level,format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    root = logging.getLogger()
    logger = logging.getLogger(__name__)
    logger.setLevel(root.level)

    ch = logging.StreamHandler()
    ch.setLevel(root.level)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)

from databasehandler import DatabaseHandler
from statehandler import StateHandler
from beeperhandler import BeeperHandler
from RFIDhandler import RFIDHandler


textlen = 24
textsize = 20

state_timeout = 120 #sec
add_credit_timeout = 60 # sec

logger.warning("GUI launching")

class GUI():
    def __init__(self,queue_state,queue_DB,queue_GUI):
        self.queue_state = queue_state
        self.queue_DB = queue_DB
        self.queue_GUI = queue_GUI

        sg.theme('Dark Brown')

        if True: # create the keyboard window
            keys = list(string.ascii_lowercase)+['Space',"Shift","Del","Clear","Submit","Exit"]

            layout = [[sg.Text('Enter your name : '), sg.Input('', size=(25, 1), key='input')],]
            layout += [
                [sg.Button('a',size=(1,1)), sg.Button('b',size=(1,1)), sg.Button('c',size=(1,1)), sg.Button('d',size=(1,1)), sg.Button('e',size=(1,1)), sg.Button('f',size=(1,1))],
                [sg.Button('g',size=(1,1)), sg.Button('h',size=(1,1)), sg.Button('i',size=(1,1)), sg.Button('j',size=(1,1)), sg.Button('k',size=(1,1)), sg.Button('l',size=(1,1))],
                [sg.Button('m',size=(1,1)), sg.Button('n',size=(1,1)), sg.Button('o',size=(1,1)), sg.Button('p',size=(1,1)), sg.Button('q',size=(1,1)), sg.Button('r',size=(1,1))],
                [sg.Button('s',size=(1,1)), sg.Button('t',size=(1,1)), sg.Button('u',size=(1,1)), sg.Button('v',size=(1,1)), sg.Button('w',size=(1,1)), sg.Button('x',size=(1,1))],
                [sg.Button('y',size=(1,1)), sg.Button('z',size=(1,1)), sg.Button("Space",size=(3,1)), sg.Button("Shift",size=(3,1)), sg.Button('Del',size=(3,1))],
                [sg.Button('Clear',size=(8,1)), sg.Button('Submit',size=(8,1)), sg.Button('Exit',size=(8,1)),],
                   ]
            self.layout_keyboard = layout
            W,H = 60,20
            self.window_keyboard = sg.Window('Keyboard', self.layout_keyboard,
                       default_button_element_size=(1, 1),
                       location=(0,0), size=(320,240),
                       #auto_size_buttons=True,
                       auto_size_buttons=False,
                       grab_anywhere=False,
                       no_titlebar=True,
                       finalize=True,
                               )
            self.window_keyboard.Hide()

            self.keys_entered = ''
            self.shift_active = False

        if True: # create the addcredit window
            layout = [[sg.Text('Amount to be credited : '), sg.Input('', size=(25, 1), key='input')],]
            layout += [
                [sg.Button('1',size=(3,1)), sg.Button('2',size=(3,1)), sg.Button('3',size=(3,1)), ],
                [sg.Button('4',size=(3,1)), sg.Button('5',size=(3,1)), sg.Button('6',size=(3,1)), ],
                [sg.Button('7',size=(3,1)), sg.Button('8',size=(3,1)), sg.Button('9',size=(3,1)), sg.Button('-',size=(3,1)),],
                [sg.Button('0',size=(3,1)),sg.Button('10',size=(3,1)),sg.Button('25',size=(3,1)),sg.Button('50',size=(3,1)), ],
                [sg.Button('Clear',size=(8,1)), sg.Button('Submit',size=(8,1)), sg.Button('Exit',size=(8,1)),],
                   ]
            self.layout_addcredit = layout
            W,H = 60,20
            self.window_addcredit = sg.Window('Add Credit', self.layout_addcredit,
                       default_button_element_size=(2, 1),
                       location=(0,0), size=(320,240),
                       #auto_size_buttons=True,
                       auto_size_buttons=False,
                       grab_anywhere=False,
                       no_titlebar=True,
                       finalize=True,
                               )
            self.window_addcredit.Hide()
            self.keys_entered = ''

        if True : #create popup window 
            self.layout_popup = [[sg.Text(fill("popup"*15,textlen),font=('Helvetica', textsize-4), justification='center',key="message")],]
            self.layout_popup += [[sg.Text('_'*40),],]
            self.layout_popup += [[sg.Button('Ok'),sg.Button('Create account')],]

            self.layout_popup = [[sg.Column(self.layout_popup,element_justification="center")]]
            self.window_popup = sg.Window('Popup', self.layout_popup,
                       default_button_element_size=(5, 2),
                       location=(0,0), size=(320,240),
                       margins=(0,0),
                       auto_size_buttons=True,
                       grab_anywhere=False,
                       finalize=True,
                       no_titlebar=True)

        if True : #create settings window 
            self.layout_settings = [
                            [sg.Button('Add UID to user')],
                            [sg.Button('Remove UID from user')],
                            [sg.Button('Exit app')],
                            [sg.Button('Back')],]

            self.window_settings = sg.Window('settings', self.layout_settings,
                       default_button_element_size=(5, 2),
                       location=(0,0), size=(320,240),
                       auto_size_buttons=True,
                       grab_anywhere=False,
                       finalize=True,
                       no_titlebar=True)

        if True : # Create the main window 
            self.layout_main = [[sg.Text('12:32:01',key='timetext',font=('Helvetica', 20), justification='center')],
                      [sg.Text(fill('Present your badge to pay for your coffee',textlen),font=('Helvetica', textsize), justification='center')],
                      [sg.T('_' * 40) ],
                      [sg.T(' ' * 55),sg.Button('Settings')],
                                ]
            self.layout_main = [[sg.Column(self.layout_main,element_justification="center")]]
            self.window_main = sg.Window('Idle', self.layout_main, 
                                    location=(0,0), size=(320,240),
                                    auto_size_buttons=True,
                                    margins=(3,3),
                                    grab_anywhere=False,
                                    finalize=True,
                                    no_titlebar=True,)
        
        self.window_popup.Hide()
        self.window_settings.Hide()
        self.window_keyboard.Hide()
        self.window_main.UnHide()

        self.timetext = self.window_main['timetext']
        self.window = self.window_main
        self.eventloop = self.eventloop_main


    def update_clock(self):
        # update the clock
        self.timetext.update(value=datetime.now().strftime("%H:%M:%S"))

    def back_to_main(self):
        self.window.close()
        self.window = self.window_main

    def eventloop_main(self):
        event, values = self.window.read(timeout=50)

        if event in ('Exit',):
            raise Exception

        if event in ('Settings',):
            self.open_settings()
        # update clock
        self.update_clock()

    def open_keyboard(self):
        if self.window is not self.window_keyboard:
            self.window_keyboard.UnHide()
            self.window.Hide()
            self.window = self.window_keyboard            
        #we go to keyboard event loop
        self.keys_entered = ''
        self.shift_active = False
        self.timeout = now()+timedelta(seconds=120)
        self.eventloop = self.eventloop_keyboard

    def eventloop_keyboard(self):
        event, values = self.window.read(timeout=50)

        if now() > self.timeout:
            self.exit_eventloop_keyboard()
            return

        elif event == "Exit":
            self.exit_eventloop_keyboard()
            entry = {"query_type":"cancel",
                     }
            self.queue_state.put(entry)
            logger.info(f"cancel state : {entry}")
            return

        elif event == 'Clear':  # clear keys if clear button
            self.keys_entered = ''

        elif event in string.ascii_lowercase:
            if self.shift_active == True:

                self.keys_entered = values['input']  # get what's been entered so far
                self.keys_entered += event.upper()  # add the new digit
                self.shift_active = False
            else :
                self.keys_entered = values['input']  # get what's been entered so far
                self.keys_entered += event  # add the new digit

        elif event == "Space":
            self.keys_entered = values['input']  # get what's been entered so far
            self.keys_entered += " "

        elif event == 'Del':
            self.keys_entered = values['input']  # get what's been entered so far
            self.keys_entered = self.keys_entered[:-1]

        elif event == "Shift":
            self.shift_active = True

        elif event == 'Submit':
            fullname = self.keys_entered
            entry = {"query_type":"add_user",
                     "badge_UID":self.last_UID,
                     "fullname":fullname,
                     "shortname":fullname}
            self.queue_DB.put(entry)
            logger.info(f"sent adduser query : {entry}")
            self.exit_eventloop_keyboard()
            return


        # change the form to reflect current key string
        self.window['input'].update(self.keys_entered)

    def exit_eventloop_keyboard(self):
        self.window_main.UnHide()
        self.window_keyboard.Hide()
        self.window = self.window_main
        self.keys_entered = ''
        self.shift_active = False
        self.window = self.window_main
        self.eventloop = self.eventloop_main

    def open_addcredit(self):
        if self.window is not self.window_addcredit:
            self.window_addcredit.UnHide()
            self.window.Hide()
            self.window = self.window_addcredit            
        #we go to keyboard event loop
        self.keys_entered = ''
        self.shift_active = False
        self.timeout = now()+timedelta(seconds=120)
        self.eventloop = self.eventloop_addcredit

    def eventloop_addcredit(self):
        event, values = self.window.read(timeout=50)

        if now() > self.timeout:
            self.exit_eventloop_addcredit()
            return

        elif event == "Exit":
            self.exit_eventloop_addcredit()
            entry = {"query_type":"cancel",
                     }
            self.queue_state.put(entry)
            logger.info(f"cancel state : {entry}")
            return

        elif event == 'Clear':  # clear keys if clear button
            self.keys_entered = ''

        elif event == "-":
            if values['input'][0] == "-":
                self.keys_entered = str(values['input'][1:])
            else:
                self.keys_entered = str("-"+values['input'][:])

        elif event in ["0","1","2","3","4","5","6","7","8","9","10","25","50","-"]:
            self.keys_entered = values['input']  # get what's been entered so far
            self.keys_entered += event  # add the new digit

        elif event == 'Submit':
            logger.info(self.keys_entered)
            try:
                amount = float(self.keys_entered)
            except ValueError:
                self.exit_eventloop_addcredit()
                entry = {"query_type":"cancel",
                         }
                self.queue_state.put(entry)
                logger.info(f"cancel state : {entry}")
                self.open_popup(f"Error : invalid amount")
                return

            entry = {"query_type":"addcredit_amount",
                     "amount":amount,
                     }
            self.queue_state.put(entry)
            logger.info(f"sent addcredit query : {entry}")
            self.exit_eventloop_addcredit()
            return


        # change the form to reflect current key string
        self.window['input'].update(self.keys_entered)

    def exit_eventloop_addcredit(self):
        self.window_main.UnHide()
        self.window_addcredit.Hide()
        self.window = self.window_main
        self.keys_entered = ''
        self.window = self.window_main
        self.eventloop = self.eventloop_main

    def open_popup(self,message="Error",timeout=10,create_account_button=False):
        self.window_popup.Element('message').Update(value = message)
        self.window_popup.Element('Create account').Update(visible = create_account_button)
        if self.window is not self.window_popup:
            self.window_popup.UnHide()
            self.window.Hide()
            self.window = self.window_popup

        self.timeout = now()+timedelta(seconds=timeout)
        logger.debug(f"timeout = {timeout}s (timestamp :{self.timeout})")
        self.eventloop = self.eventloop_popup
        #timeout in seconds

    def eventloop_popup(self):
        event, values = self.window.read(timeout=100) #timeout in ms

        if event == "Exit":
            raise Exception

        elif now() > self.timeout:
            self.exit_eventloop_popup()

        elif event == 'Ok':
            self.exit_eventloop_popup()

        elif event == 'Create account':
            self.exit_eventloop_popup()
            self.open_keyboard()

    def exit_eventloop_popup(self):
        self.window_main.UnHide()
        self.window_popup.Hide()
        self.window = self.window_main
        self.eventloop = self.eventloop_main

    def open_settings(self):
        if self.window is not self.window_settings:
            self.window_settings.UnHide()
            self.window.Hide()
            self.window = self.window_settings
            
        self.timeout = now()+timedelta(seconds=60)
        self.eventloop = self.eventloop_settings

    def eventloop_settings(self):
        event, values = self.window.read(timeout=100) #timeout in ms

        if event == "Exit app":
            raise Exception

        elif now() > self.timeout:
            self.exit_eventloop_settings()

        elif event == 'Add UID to user':
            entry = {"query_type":"GUI_query",
                     "message":"add_UID_to_user",
                     }
            self.queue_state.put(entry)
            logger.info(f"sent add_UID_to_user query : {entry}")

        elif event == 'Remove UID from user':
            entry = {"query_type":"GUI_query",
                     "message":"remove_UID_from_user",
                     }
            self.queue_state.put(entry)
            logger.info(f"sent remove_UID_from_user query : {entry}")

        elif event == 'Back':
            self.exit_eventloop_settings()
            return

    def exit_eventloop_settings(self):
        self.window_main.UnHide()
        self.window_settings.Hide()
        self.window = self.window_main
        self.eventloop = self.eventloop_main

def main():
    #initialize all the component threads and queues
    queue_state = queue.Queue()
    queue_DB = queue.Queue()
    queue_GUI = queue.Queue()
    queue_beeper = queue.Queue()

    statehandler = StateHandler(queue_state,queue_GUI,queue_DB,queue_beeper)
    databasehandler = DatabaseHandler(queue_state,queue_DB)
    rfidhandler = RFIDHandler(queue_state)
    beeperhandler = BeeperHandler(queue_beeper)

    statehandler.state_timeout = state_timeout
    statehandler.add_credit_timeout = add_credit_timeout 

    statehandler.setDaemon(True)
    databasehandler.setDaemon(True)
    rfidhandler.setDaemon(True)
    beeperhandler.setDaemon(True) 

    statehandler.start()
    databasehandler.start()
    beeperhandler.start()
    rfidhandler.start()



    # Get the GUI object that is used to update the window
    gui = GUI(queue_state,queue_DB,queue_GUI)

    # ---------- EVENT LOOP ----------
    event, values = None,None
    try :
        while True:
            # Poll queue
            try:
                queue_event = gui.queue_GUI.get(block=False)
                logger.debug(queue_event)
            except queue.Empty:
                pass
            else:
                if queue_event["query_type"] == "cancel":
                    if gui.window is not gui.window_main:
                        gui.window_main.UnHide()
                        time.sleep(0.1)
                        gui.window.Hide()
                        gui.window = gui.window_main
                        gui.eventloop = gui.eventloop_main

                if queue_event["query_type"] == "DB_answer":
                    if queue_event["message"] == "UID_unknown":
                        gui.last_UID = queue_event["UID"]
                        gui.open_popup("No account is associated to that badge",
                                        create_account_button = True,
                                        timeout=10,)
                    elif queue_event["message"]  == "transaction_OK":
                        gui.open_popup(f"{queue_event['fullname']} paid 1 coffee, remaining balance:{int(queue_event['balance'])}",)
                    elif queue_event["message"]  == "transaction_OK_negative_balance":
                        gui.open_popup(f"{queue_event['fullname']} paid 1 coffee, balance:{int(queue_event['balance'])}, find a supervisor to refill your account",)
                    elif queue_event["message"]  == "transaction_add_credit_done":
                        gui.open_popup(f"+{int(queue_event['added_credits'])} credits for {queue_event['fullname']}, Account balance : {int(queue_event['balance'])}",)
                    elif queue_event["message"]  == "user_added":
                        gui.open_popup(f"New account created, Name:{queue_event['fullname']}, BadgeID:{queue_event['UID']}",)
                    elif queue_event["message"]  == "UID_added":
                        gui.open_popup(f"BadgeID:{queue_event['UID']} added to acount: {queue_event['fullname']} ",)
                    elif queue_event["message"]  == "UID_removed":
                        gui.open_popup(f"BadgeID:{queue_event['UID']} removed from acount: {queue_event['fullname']} ",)
                    #elif queue_event["message"]  == #change state:
                    elif queue_event["message"]  == "transaction_add_credit_initiated":
                        #gui.open_popup(f"Please present the badge for the account to be credited",timeout=add_credit_timeout)
                        gui.open_addcredit()
                    #elif queue_event["message"]  == #errors:
                    elif queue_event["message"]  == "transaction_NOK":
                        gui.open_popup(f"Error: transaction failed",)
                    elif queue_event["message"]  == "UID_invalid":
                        gui.open_popup(f"Error: Badge ID is invalid",)
                    elif queue_event["message"]  == "UID_taken":
                        gui.open_popup(f"Error: Badge ID is already taken",)
                    elif queue_event["message"]  == "name_taken":
                        gui.open_popup(f"Error: Name is already taken",)
                    elif queue_event["message"]  == "missing_data":
                        gui.open_popup(f"Error: Some data was missing when processing the query",)
                    elif queue_event["message"] == "UID_unknown":
                        gui.open_popup(f"Error: Badge ID is unknown",)

                    else :
                        gui.open_popup(f"Error : {queue_event['message']}")

                if queue_event["query_type"] == "alert":
                    timeout = queue_event.get('timeout',10)
                    gui.open_popup(queue_event['message'],timeout=timeout)

            gui.eventloop()

    except (Exception,KeyboardInterrupt) as e:
        statehandler.stop()
        databasehandler.stop()
        beeperhandler.stop()
        rfidhandler.stop()


        gui.window_main.close()
        gui.window_popup.close()
        gui.window_settings.close()
        gui.window_keyboard.close()
        gui.window_addcredit.close()
        time.sleep(0.5)
        raise e


if __name__ == '__main__':
    main()
