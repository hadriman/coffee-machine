import queue
import logging
import threading
import time
import re

try:
    import RPi.GPIO as GPIO
    simulation_mode = False
except (RuntimeError,ImportError):
    simulation_mode = True

root = logging.getLogger()
logger = logging.getLogger(__name__)
logger.setLevel(root.level)

ch = logging.StreamHandler()
ch.setLevel(root.level)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

class BeeperHandler(threading.Thread):
    """
    mini thread to handle the beeps
    """
    def __init__(self,queue_beeper):
        super().__init__()
        self.queue_beeper = queue_beeper

        #we only set up the GPIO if the beeper is enabled
        #GPIO.setmode(GPIO.BOARD)
        GPIO.setup(12, GPIO.OUT)

    def beep(self,beep_phrase):
        """
        by default if we receive any event we'll do a single short bip
        """
        beeps = re.findall("b.*p",beep_phrase)
        if len(beeps) == 0:
            #the phrase wasn't a valid beep pharse, we'll still beep
            logger.info(f"not a valid beep_phrase : {beep_phrase}")
            beeps = ["bip",]

        for beep in beeps:
            if beep == "bip":
                p = GPIO.PWM(12, 1000)
            if beep == "beep":
                p = GPIO.PWM(12, 500)
            if beep == "boop":
                p = GPIO.PWM(12, 200)
            p.start(0)
            logger.debug(f"{beep}")
            time.sleep(0.1)
            p.stop()
            time.sleep(0.1)

    def run(self):
        while True:
            try:
                queue_event = self.queue_beeper.get_nowait()
                logger.debug(f"beep event received {queue_event}")
            except queue.Empty:     # get_nowait() will raise exception when Queue is empty
                time.sleep(0.1)
                continue
            
        if "sound" in queue_event.keys():
            self.beep(queue_envent["sound"])


        else :
            self.beep(queue_event["query_type"])

    def stop(self):
        GPIO.cleanup()

if simulation_mode == True:
    class BeeperHandler(threading.Thread):
        def __init__(self,queue_beeper):
            super().__init__()
            self.queue_beeper = queue_beeper
            logger.critical(f"Starting beeper in simulation mode")


        def beep(self,beep_phrase):
            beeps = re.findall("b.*p",beep_phrase)
            if len(beeps) == 0:
                #the phrase wasn't a valid beep pharse, we'll still beep
                logger.info(f"not a valid beep_phrase : {beep_phrase}")

        def run(self):
            while True:
                try:
                    queue_event = self.queue_beeper.get_nowait()
                    logger.debug(f"beep event received {queue_event}")
                except queue.Empty:     # get_nowait() will raise exception when Queue is empty
                    time.sleep(0.1)
                    continue
                
            if "sound" in queue_event.keys():
                self.beep(queue_envent["sound"])


            else :
                self.beep(queue_event["query_type"])

        def stop(self):
            pass
